from django.db import models

class Hats(models.Model):
    style_name = models.CharField(max_length=100)
    fabric = models.CharField(max_length=100)
    color = models.CharField(max_length=50)
    hat_picture_url = models.URLField(null=True)

    location = 
