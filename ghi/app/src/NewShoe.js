import React, {useEffect, useState } from 'react';

function NewShoeForm() {


    const [bin, setBin] = useState('');
    const [name, setName] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [bins, setBins] = useState([]);
    const [imgUrl, setImgUrl] = useState('');
    const [color, setColor] = useState('');


    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
        const data = await response.json();
        setBins(data.bins);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.bin = bin;
        data.name = name;
        data.manufacturer = manufacturer;
        data.color = color;
        data.image_url = imgUrl;
        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchOptions = {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
        };
        const shoeResponse = await fetch(shoeUrl, fetchOptions);
        if (shoeResponse.ok) {

        setBin('');
        setName('');
        setManufacturer('');
        setColor('');
        setImgUrl('');

        }
    }

    const handleChangeBin = (event) => {
        const value = event.target.value;
        setBin(value);
    }

    const handleChangeName = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleChangeManufacturer = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }
    const handleChangeColor = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const handleChangeImgUrl = (event) => {
        const value = event.target.value;
        setImgUrl(value);
    }


    // CSS classes for rendering
    // let spinnerClasses = 'd-flex justify-content-center mb-3';
    // let dropdownClasses = 'form-select d-none';
    // if (bins.length > 0) {
    //     spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
    //     dropdownClasses = 'form-select';
    // }

    // let messageClasses = 'alert alert-success mb-0';
    // let formClasses = 'd-none';

    return (
        <div className="my-5 container">
        <div >
            <div >
            <div className="card shadow">
                <div className="card-body">
                <form onSubmit={handleSubmit} id="create-shoe-form">
                    <h1 className="card-title">Organize your shoes!</h1>
                    <p className="mb-3">
                    Please choose which bin
                    you'd like to put your shoes in.
                    </p>
                    <div className="mb-3">
                    <select onChange={handleChangeBin}  value={bin} name="bin" id="bin" className='form-select' required>
                        <option value="">Choose a bin</option>
                        {bins.map(bin => {
                        return (
                            <option key={bin.href} value={bin.href}>{bin.closet_name}</option>
                        )
                        })}
                    </select>
                    </div>
                    <p className="mb-3">
                    Describe your shoe here.
                    </p>
                    <div >
                    <div >
                        <div className="form-floating mb-3">
                        <input onChange={handleChangeManufacturer} value={manufacturer} placeholder="Shoe manufacturer name here" required type="text" id="manufacturer" name="manufacturer" className="form-control" />
                        <label htmlFor="manufacturer">Manufacturer/Brand name</label>
                        </div>
                    </div>
                    <div >
                        <div className="form-floating mb-3">
                        <input onChange={handleChangeName} value={name} placeholder="Shoe name" required type="text" id="name" name="name" className="form-control" />
                        <label htmlFor="name">Your full name</label>
                        </div>
                    </div>
                    <div >
                        <div className="form-floating mb-3">
                        <input onChange={handleChangeColor} value={color} placeholder="Primary color of shoe" required type="text" id="color" name="color" className="form-control" />
                        <label htmlFor="color">Your shoe's primary color</label>
                        </div>
                    </div>
                    <div >
                        <div className="form-floating mb-3">
                        <input onChange={handleChangeImgUrl} value={imgUrl} placeholder="Image URL" required type="url" id="imgUrl" name="imgUrl" className="form-control" />
                        <label htmlFor="imgUrl">Upload a URL link to a picture of your shoe</label>
                        </div>
                    </div>
                    </div>
                    <button className="btn btn-lg btn-primary">Add shoe to bin!</button>
                </form>
                </div>
            </div>
            </div>
            </div>
            </div>
    );
    }

export default NewShoeForm;
