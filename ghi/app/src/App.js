import { BrowserRouter, Routes, Route } from 'react-router-dom';

import MainPage from './MainPage';
import Nav from './Nav';
import NewShoeForm from './NewShoe';
import ShoesList from './ShoesList';

function App(props) {
  if(props===undefined){
    return null
  }

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes" element={<ShoesList />} />
          <Route path="shoes" >
              <Route path="new" element={<NewShoeForm />} />
          </Route>

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
