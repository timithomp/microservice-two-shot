import React, {useEffect, useState} from "react";
import { Link } from "react-router-dom";

function ShoesList(props) {

    const [shoes, setShoes]=useState([])

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/shoes/';
        const response = await fetch(url);
        if (response.ok) {
        const data = await response.json();
        setShoes(data.shoes);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <>
        <Link to="/shoes/new/" className="btn btn-primary" >Add a new shoe to your bin!</Link>
        <table className="table table-striped table-bordered">
        <thead>
            <tr>
            <th > Manufacturer</th>
            <th > Name </th>
            <th > Color </th>
            <th > Image </th>
            <th > Bin </th>
            </tr>
        </thead>
        <tbody>
            {shoes.map((shoe) => {
            return (
                <tr key = { shoe.href }>
                    <td >{ shoe.manufacturer }</td>
                    <td >{ shoe.name }</td>
                    <td >{ shoe.color }</td>
                    <td >
                        <img
                        src={ shoe.image_url }
                        className="img-thumbnail"
                        alt= "shoe"
                        />
                    </td>
                    <td >{ shoe.bin }</td>
                    <td >
                        <button onClick={
                        async (event)=>{
                            const shoeUrl = `http://localhost:8080/api/shoes/${ shoe.id }`;
                            const fetchOptions = {
                                method: 'delete',
                            };
                            const shoeResponse = await fetch(shoeUrl, fetchOptions);
                            if (shoeResponse.ok) {
                                window.location.reload()
                            }
                        }
                     } className="btn btn-lg btn-primary">Delete this shoe</button>
                    </td>
                </tr>
            );
        })}
        </tbody>
        </table>
        </>
    );
}

export default ShoesList;
